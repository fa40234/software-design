<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>

<body>
<?php
include('header.php');
?>
<?php require_once 'dokumentiUpload.php'?>
    <div class="mx-auto">
        <div class="container-fluid">
            <div class="mx-auto">
                <div class="col-md-4">
                    <div class="form-row">
                        <h2>Upload File</h2>
                    </div>
                    <form action="dokumentiUpload.php" method="POST" enctype="multipart/form-data">
                        <p>Name:</p>
                        <input type="text" name="name" class="form-control">
                        <p>datar:</p>
                        <input type="date" name="data_r" class="form-control">
                        <br>
                        <input type="file" name="uploadfile" class="form-control" />
                        <input type="submit" name="uploadfilesub" value="Upload" class="btn btn-primary btn-block"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>



</html>